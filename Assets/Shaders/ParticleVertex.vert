#version 410

layout(location = 0) in vec4 Position;
layout(location = 2) in vec2 TexCoord;
//in vec4 Colour;

out vec2 vTexCoord;

uniform mat4 ProjectionViewModel;

void main() 
{
	vTexCoord = TexCoord;
	//vColour = Colour;
	gl_Position = ProjectionViewModel * Position;
}
#version 410

in vec2 vTexCoord; //the vertex tex coord

uniform sampler2D noiseTexture; //the perlin noise texture provided
uniform sampler2D grassTexture; //the grass texture that is used as the default texture
uniform sampler2D rockTexture; //the rock texture used as the height texture

out vec4 FragColour;


void main() 
{
	vec4 noise = texture(noiseTexture,vTexCoord); //sample the noise texture

	float backTextureAmount = 1 - (noise.r + noise.g + noise.b); //get the black texture


	vec4 grassColour = texture(grassTexture, vTexCoord * 40) * backTextureAmount; //have it so grass draws on the black texture
	vec4 rockColour = texture(rockTexture, vTexCoord * 40) * noise.r; //have it so rocks draw on the noise red pixels

	//pass out colour
	FragColour = grassColour  + rockColour;
}

#version 410

in vec2 vTexCoord;

uniform sampler2D depthTarget;
uniform sampler2D colourTarget;
uniform vec3 cameraPosition;
uniform float lineThickness;
uniform float focus;
uniform float focusNear;
uniform float focusFar;

uniform bool depthOfField;
uniform bool boxBlur;
uniform bool distortion;
uniform bool edgeDetection;
uniform bool grayScale;
uniform bool speia;




out vec4 FragColour;

vec4 Default(vec2 texCoord) {
		return texture(colourTarget, texCoord);
}

vec4 BoxBlur(vec2 texCoord)
{
	vec2 texel = 1.0f / textureSize(colourTarget, 0);

	vec4 colour = texture(colourTarget, texCoord);
	colour += texture(colourTarget, texCoord + texel * vec2(-2,2));
	colour += texture(colourTarget, texCoord + texel * vec2(-2,0));
	colour += texture(colourTarget, texCoord + texel * vec2(-2,-2));
	colour += texture(colourTarget, texCoord + texel * vec2(0,2));
	colour += texture(colourTarget, texCoord + texel * vec2(0,-2));
	colour += texture(colourTarget, texCoord + texel * vec2(2,2));
	colour += texture(colourTarget, texCoord + texel * vec2(2,0));
	colour += texture(colourTarget, texCoord + texel * vec2(2,-2));

	return colour / 9;

}

vec4 Distortion(vec2 texCoord)
{
	vec2 mid = vec2(0.5f);

	float distanceFromCentre = distance(texCoord, mid);
	vec2 normalizedCoord = normalize(texCoord - mid);
	float bias = distanceFromCentre + sin(distanceFromCentre * 15) * 0.05f;

	vec2 newCoord = mid + bias * normalizedCoord;
	return texture(colourTarget, newCoord);
}

vec4 EdgeDetection(vec2 texCoord)
{
	//using sobels operator
	mat3 kernelX = mat3(	
		-1,0,1,
		-2,0,2,
		-1,0,1
	);
	mat3 kernelY = mat3(
		-1,-2,-1,
		0,0,0,
		1,2,1
	);

	//Grab the diffuse
	vec3 diffuse = texture(colourTarget, vTexCoord.st).rgb;
	mat3 I; 
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
		//fetch the current texels coordinates and apply an offset.
			vec3 sampleEdge = texelFetch(colourTarget, ivec2(gl_FragCoord) + ivec2(i-1,j-1), 0).rgb;
			I[i][j] = length(sampleEdge); //set the matrix to the output of temp
		}
	}

	//get the x and y coords of the edges
	float sx = dot(kernelX[0], I[0]) + dot(kernelX[1], I[1]) + dot(kernelX[2], I[2]);
	float sy = dot(kernelY[0], I[0]) + dot(kernelY[1], I[1]) + dot(kernelY[2], I[2]);

	float sobel = sqrt(pow(sx, lineThickness) + pow(sy,lineThickness)); //x^2 + y^2

	vec4 colour = vec4(diffuse - vec3(sobel),1); //set the colour

	return colour;
}

vec4 Edge(vec2 texCoord, float strength)
{
	vec2 texel = 1.0f / textureSize(colourTarget, 0);

	vec4 colourX;
	colourX += -1 * texture(colourTarget, texCoord + texel * vec2(-strength,strength));
	colourX += -2 * texture(colourTarget, texCoord + texel * vec2(-strength,0));
	colourX += -1 * texture(colourTarget, texCoord + texel * vec2(-strength,-strength));
	colourX += 1 * texture(colourTarget, texCoord + texel * vec2(strength,strength));
	colourX += 2 * texture(colourTarget, texCoord + texel * vec2(strength,0));
	colourX += 1 * texture(colourTarget, texCoord + texel * vec2(strength,-strength));
	vec4 colourY;
	colourY += -1 * texture(colourTarget, texCoord + texel * vec2(-strength,strength));
	colourY += 1 * texture(colourTarget, texCoord + texel * vec2(-strength,-strength));
	colourY += -2 * texture(colourTarget, texCoord + texel * vec2(strength,strength));
	colourY += 2 * texture(colourTarget, texCoord + texel * vec2(-strength,-strength));
	colourY += -1 * texture(colourTarget, texCoord + texel * vec2(strength, strength));
	colourY += -1 * texture(colourTarget, texCoord + texel * vec2(strength,-strength));


	return sqrt(colourX * colourX + colourY * colourY);

}


vec4 GrayScale(vec2 texCoord)
{
	vec4 colour = texture(colourTarget, texCoord);
	float gray = (colour.r + colour.b + colour.g) / 3;
	vec4 grayscale = vec4(gray, gray, gray,1);

	return grayscale;
}

vec4 Sepia(vec2 texCoord)
{
	vec4 colour = texture(colourTarget, texCoord);
	float sepiaR = (0.393f * colour.r + 0.769f * colour.g + 0.189f * colour.b);
	float sepiaG = (0.349f * colour.r + 0.686f * colour.g + 0.168f * colour.b);
	float sepiaB = (0.272f * colour.r + 0.534f * colour.g + 0.131f * colour.b);

	vec4 sepia = vec4(sepiaR, sepiaG, sepiaB, 1);
	return sepia;
}

float calculateBlurStrength(float depth)
{
	float f;
	if (depth < focus)
	{
		f = (depth - focus) / (focusNear);
	}
	else
	{
		f = (depth - focus) / (-focusFar);
	}
	return min(1, abs(f));
}



vec4 DepthOfField(vec2 texCoord)
{
	vec2 texel = 1.0f / textureSize(colourTarget, 0);
	vec3 distance = cameraPosition;
	float depth = length(gl_FragDepth);


	float blurStrength = calculateBlurStrength(texture(depthTarget, texCoord).r);
	vec4 colour = texture(depthTarget, texCoord);	
	colour += texture(colourTarget, texCoord + texel * vec2(-blurStrength,blurStrength));
	colour += texture(colourTarget, texCoord + texel * vec2(-blurStrength,0));
	colour += texture(colourTarget, texCoord + texel * vec2(-blurStrength,-blurStrength));
	colour += texture(colourTarget, texCoord + texel * vec2(0,blurStrength));
	colour += texture(colourTarget, texCoord + texel * vec2(0,-blurStrength));
	colour += texture(colourTarget, texCoord + texel * vec2(blurStrength,blurStrength));
	colour += texture(colourTarget, texCoord + texel * vec2(blurStrength,0));
	colour += texture(colourTarget, texCoord + texel * vec2(blurStrength,-blurStrength));
	return colour / 9;
}



void main()
{
	vec2 texSize = textureSize(colourTarget, 0);
	vec2 texelSize = 1.0f / texSize;

	vec2 scale = (texSize - texelSize) / texSize;
	vec2 texCoord = vTexCoord / scale + texelSize * 0.5f;

	vec2 depthTexCoord = vTexCoord;


	if (!depthOfField && !boxBlur && !distortion && !edgeDetection && grayScale && !speia)
	{
		FragColour = Default(texCoord);
	}
	//if (boxBlur)
	{
	//	FragColour = Default(texCoord) + BoxBlue(texCoord);
//	}


	//FragColour = DepthOfField(depthTexCoord);
	//FragColour = Default(texCoord);



}


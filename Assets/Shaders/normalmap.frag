#version 410

//vertex info passed in from the vertex shader
in vec2 vTexCoord;
in vec3 vNormal;
in vec3 vTangent;
in vec3 vBiTangent;
in vec4 vPosition;

out vec4 FragColour; //the colour we output

//pass in gameObjects material textures
uniform sampler2D diffuseTexture;
uniform sampler2D specularTexture;
uniform sampler2D normalTexture;
uniform sampler2D depthTexture;


uniform vec3 Ka; //Ambient value
uniform vec3 Kd; //Diffuse value
uniform vec3 Ks; //Specular value
uniform float specularPower; //Power of the specular

uniform vec3 Ia; //Ambient light colour
uniform vec3 Id; //Diffuse light colour
uniform vec3 Is; //Specular light colour
uniform vec3 LightDirection; 

uniform vec3 cameraPosition; //the main cameraPosition
uniform vec3 colour; //the colour of the object

uniform float roughness; 
uniform float reflectionCoefficient; //how shiny the object is

//fetching all the light sources
uniform int numLights;
uniform vec3 lightPos[4];
uniform vec3 lightColour[4];
uniform float lightPower[4];

//Using Oran Nayars method of diffuse reflectance
float getDiffuse(vec3 N, vec3 E, vec3 L)
{
	float NdL = max(0.0f, dot(N,L));
	float NdE = max(0.0f, dot(N,E));
	
	float R2 = roughness * roughness;
	
	float A = 1.0f - 0.5f * R2 / (R2 + 0.33f);
	float B2 = 0.45f * R2 / (R2 + 0.09f);
	
	//CX = Max(0, cos(l,e))
	vec3 lightProjected = normalize(L - N * NdL);
	vec3 viewProjected = normalize(E - N * NdE);
	float CX = max(0.0f, dot(lightProjected, viewProjected));
	
	//DX = sin(alpha) * tan(beta)
	float alpha = sin(max(acos(NdE), acos(NdL)));
	float beta =  tan(max(acos(NdE), acos(NdL)));
	float DX = alpha * beta;
	
	float OrenNayar = NdL * (A + B2 * CX * DX);
	

	return OrenNayar;
}

//Using Cook-Torrance method of specular reflectance
float getSpecular(vec3 N, vec3 E, vec3 L)
{
	vec3 H = normalize(L + E);

	/////COOK-TORRANCE SPECULAR REFLECTANCE///////
	float NdH = max(0.0f, dot(N,H));
	float NdE = max(0.0f, dot(N,E));
	float NdL = max(0.0f, dot(N,L));
	float NdH2 = NdH * NdH;
	float e = 2.71828182845904523536028747135f;
	float pi = 3.1415926535897932384626433832f;
	
	////Beckman's Distribution Function D
	float R2 = roughness * roughness;
	float exponent = -(1 - NdH2) / (NdH2 * R2);
	float D = pow(e,exponent) / (R2 * NdH2 * NdH2);
	
	////Fresnel Term F
	float F = reflectionCoefficient + (1 - reflectionCoefficient) * pow(1 - NdE, 5);
	
	////Geometric Attenuation Factor G
	float X = 2.0f * NdH / dot(E,H);
	float G = min(1, min(X * NdL, X * NdE));
	
	float CookTorrance = max((D*G*F) / (NdE * pi), 0.0f);

	return CookTorrance;
}

void main()
{
	//Normalize vectors to all be unit length
	vec3 N = normalize(vNormal);
	vec3 T = normalize(vTangent);
	vec3 B = normalize(vBiTangent);

	//Tangent Basis Matrix
	mat3 TBN = mat3(T, B, N);
	//sample material textures
	vec3 texDiffuse = texture(diffuseTexture, vTexCoord).rgb * colour;
	vec3 texSpecular = texture(specularTexture, vTexCoord).rgb;
	vec3 texNormal = texture(normalTexture, vTexCoord).rgb;

	vec3 ambient = vec3(0,0,0);
	vec3 diffuse = vec3(0,0,0);
	vec3 specular = vec3(0,0,0);

	//correct normal from -1 to 1 range to the 0 to 2 range and transform it by the Tangent Basis Matrix
	N = TBN * (texNormal * 2 - 1);

	vec3 V = normalize(cameraPosition - vPosition.xyz);

	vec3 E = V;

	//loop through all lights and apply light colour and power the objects diffuse and specular
	for (int i = 0; i < numLights; i++)
	{
		//calculate light direction
		vec3 lightDir = lightPos[i] - vPosition.xyz;

		//calculate the reduction of the lights strength based on how far it is from the object
		float attentuation = 1.0f/dot(lightDir, lightDir);

		//make it unit length
		lightDir = normalize(lightDir);

		vec3 L = lightDir;
		//apply diffuse
		diffuse += getDiffuse(N, E, L) * lightColour[i] * lightPower[i] * attentuation;
		//apply specular
		specular += getSpecular(N, E, L) * lightColour[i] * lightPower[i] * 0.1f;
	}

	//set ambient colour
	ambient += Ia * Ka * texDiffuse;
	//set diffuse colour
	diffuse += Id * Kd * texDiffuse;
	//set specular colour
	specular += Is * Ks * texSpecular;

	//return texture
	FragColour = vec4(ambient + diffuse + specular, 1);

}
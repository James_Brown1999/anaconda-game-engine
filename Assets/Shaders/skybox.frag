#version 410

out vec4 FragColor; //whats being displayed on the mesh

in vec3 TexCoords; //tex coords

uniform samplerCube skybox; //cubemap

void main()
{
	FragColor = texture(skybox, TexCoords); //sample the cube map at the TexCoords
}




#version 410

layout(location = 0) in vec4 Position;
layout(location = 2) in vec2 TexCoord;
layout(location = 3) in vec2 prevTexCoord;


out vec2 vTexCoord;
out vec2 previous_vTexCoords;

uniform mat4 ProjectionViewModel;

void main()
{
	vTexCoord = TexCoord;
	previous_vTexCoords = prevTexCoord;
	gl_Position = ProjectionViewModel * Position;
}
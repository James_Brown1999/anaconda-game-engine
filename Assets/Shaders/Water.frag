#version 410


in vec2 vTexCoord;

uniform sampler2D targetTexture;

out vec4 FragColour;


void main() 
{
	FragColour = texture( targetTexture, vTexCoord );
}

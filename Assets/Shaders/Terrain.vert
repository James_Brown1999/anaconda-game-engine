#version 410

layout( location = 0 ) in vec4 Position; //position of the vertex
layout( location = 2 ) in vec2 TexCoord; //tex coord
out vec2 vTexCoord; //the tex coord we will pass to the fragment shader

uniform sampler2D noiseTexture; //the perlin noise texture provided
uniform float noiseScale; //control the scale of the noise

uniform mat4 ProjectionViewModel; //the ProjectionViewModel matrix


void main() 
{
	vTexCoord = TexCoord; //assign out TexCoord
	//get height based on how much red is in the texture at the current texcoord
	float height = texture(noiseTexture, vTexCoord).r * noiseScale; 
	//apply height offset to the vertex
	gl_Position = ProjectionViewModel * (Position + vec4(0,height,0,0));
}
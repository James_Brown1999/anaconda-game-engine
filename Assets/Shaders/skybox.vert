#version 410

layout (location = 0) in vec3 Position; //position of the cube
out vec3 TexCoords; //TexCoords passing to the frag shader

uniform mat4 ProjectionViewModel; //camera view

void main()
{
	TexCoords = Position; //3d position can be used as its a cubemap.
	gl_Position = ProjectionViewModel * vec4(Position,1.0);
}





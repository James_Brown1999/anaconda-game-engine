#include "Audio.h"


Audio::Audio()
{
	Init();
}


Audio::~Audio()
{
	ShutDown();
}

//Initalize and create the audio source
void Audio::Init()
{
	implementation = new Implementation;
}

//called everyframe if audio source is implemented
void Audio::Update()
{
	implementation->Update();
}

void Audio::ShutDown()
{
	delete implementation;
}

//used to check if an Fmod operation was successful
int Audio::ErrorCheck(FMOD_RESULT result)
{
	//if there is an error, log error in console
	if (result != FMOD_OK)
	{
		Debug::Log("FMOD ERROR " + result);
		return 1;
	}

	//log success message.
	Debug::Log("FMOD SOUND LOADED");
	return 0;
}

//load bank
void Audio::LoadBank(const string & strBankName, FMOD_STUDIO_LOAD_BANK_FLAGS flags)
{
	//find bank name
	auto foundIt = implementation->mBanks.find(strBankName);
	//bank name doesnt equal end element, exit
	if (foundIt != implementation->mBanks.end())
		return;

	//create an Fmod bank
	FMOD::Studio::Bank* pBank;
	//load bank file an check result
	Audio::ErrorCheck(implementation->mpStudioSystem->loadBankFile(strBankName.c_str(), flags, &pBank));
	if (pBank) //bank was loaded
	{
		//store it
		implementation->mBanks[strBankName] = pBank;
	}
}
//load event
void Audio::LoadEvent(const string & strEventName)
{
	//find event by name
	auto foundIt = implementation->mEvents.find(strEventName);
	//event doesnt equal end element, exit
	if (foundIt != implementation->mEvents.end())
		return;

	//create an FMOD event description
	FMOD::Studio::EventDescription* eventDescription = NULL;
	//fetch event description
	Audio::ErrorCheck(implementation->mpStudioSystem->getEvent(strEventName.c_str(), &eventDescription));
	if (eventDescription) //event description was loaded
	{
		//create an FMOD event instance
		FMOD::Studio::EventInstance* eventInstance = NULL;
		//create event
		Audio::ErrorCheck(eventDescription->createInstance(&eventInstance));
		if (eventInstance) //event was created successfully
		{
			//store it
			implementation->mEvents[strEventName] = eventInstance;
		}
	}
}

void Audio::LoadSound(const string & strSoundName, bool _3d, bool _looping)
{
	//find sound by name
	auto FoundIt = implementation->mSounds.find(strSoundName);
	//sound doesnt equal end element, exit
	if (FoundIt != implementation->mSounds.end())
		return;

	//create audio mode
	FMOD_MODE mode = FMOD_DEFAULT;

	//set mode to 3d audio
	if (_3d)
	{
		mode = FMOD_3D;
	}
	else //set mode to 2d
	{
		mode = FMOD_2D;
	}
	//add loop mode setting
	if (_looping)
	{
		mode += FMOD_LOOP_NORMAL;
	}
	
	
	FMOD::Sound* sound = nullptr;
	//create and load FMOD sound
	Audio::ErrorCheck(implementation->mpSystem->createSound(strSoundName.c_str(), mode, nullptr, &sound));

	if (sound) //sound was loaded
	{
		//store it
		implementation->mSounds[strSoundName] = sound;
	}


}
//unloading sounds
void Audio::UnloadSound(const string & strSoundName)
{
	//find sound
	auto foundIt = implementation->mSounds.find(strSoundName);
	//sound equals to end, exit
	if (foundIt == implementation->mSounds.end())
		return;

	//release sound from memory
	Audio::ErrorCheck(foundIt->second->release());
	//delete from map
	implementation->mSounds.erase(foundIt);


}

//set the fmod 3d listener attributes
void Audio::Set3dListenerPos(const vec3 & pos, const vec3& forward, const vec3& up, float volume)
{
	
	FMOD_VECTOR p = VectorToFmod(pos); //set FMOD vector position
	FMOD_VECTOR v = VectorToFmod(vec3(0)); //not doing velocity atm.
	FMOD_VECTOR f = VectorToFmod(forward); //set forward FMOD vector position
	FMOD_VECTOR u = VectorToFmod(up); //set up FMOD vector position

	//attempt to set the 3d listener attributes
	Audio::ErrorCheck(implementation->mpSystem->set3DListenerAttributes(0, &p, &v, &f, &u));
}

//play sound
int Audio::PlaySound(const string & strSoundName, const vec3 & pos, float volume)
{
	//get the channelID
	int channelId = implementation->mnNextChannelId++;
	//get the sound
	auto foundIt = implementation->mSounds.find(strSoundName);
	if (foundIt == implementation->mSounds.end()) //sound isnt in our sound map
	{
		LoadSound(strSoundName); //load the sound
		foundIt = implementation->mSounds.find(strSoundName);// try to find the sound
		if (foundIt == implementation->mSounds.end()) //sound wasnt loaded correctly
		{
			return channelId; //exit
		}
	}
	FMOD::Channel* channel = nullptr; //create an FMOD channel and attempt to play the sound
	Audio::ErrorCheck(implementation->mpSystem->playSound(foundIt->second, nullptr, true, &channel));
	//chanel was created successfully
	if (channel)
	{
		//fetch mode
		FMOD_MODE mode;
		foundIt->second->getMode(&mode);
		if (mode && FMOD_3D) //mode exists and is 3d
		{
			//set 3d attributes
			FMOD_VECTOR position = VectorToFmod(pos);
			Audio::ErrorCheck(channel->set3DAttributes(&position, nullptr));
		}
		//set the volume
		Audio::ErrorCheck(channel->setVolume(dbToVolume(volume)));
		//set whether it is paused
		Audio::ErrorCheck(channel->setPaused(false));
		//store the channel in channel map.
		implementation->mChannels[channelId] = channel;
	}
	//store the channel count (ID)
	implementation->channelCount = channelId;
	return channelId;
}

void Audio::PlayEvent(const string & strEventName)
{
	//search for the event in event map
	auto foundIt = implementation->mEvents.find(strEventName);
	//if event wasnt loaded
	if (foundIt == implementation->mEvents.end())
	{
		//load event
		LoadEvent(strEventName);
		//check if it loaded correctly
		foundIt = implementation->mEvents.find(strEventName);
		// it hasnt exit
		if (foundIt == implementation->mEvents.end())
			return;
	}
	//play it
	foundIt->second->start();
}


void Audio::StopEvent(const string & strEventName, bool immediate)
{
	//find event in map
	auto foundIt = implementation->mEvents.find(strEventName);
	//event wasnt found
	if (foundIt == implementation->mEvents.end())
		return;

	//create mode
	FMOD_STUDIO_STOP_MODE mode;
	//set the mode
	mode = immediate ? FMOD_STUDIO_STOP_IMMEDIATE : FMOD_STUDIO_STOP_ALLOWFADEOUT;
	//attempt to stop it
	Audio::ErrorCheck(foundIt->second->stop(mode));
}

//set the 3d position
void Audio::SetChannel3dPosition(int nChannelId, const vec3 & pos)
{
	//attempt to find channel
	auto foundIt = implementation->mChannels.find(nChannelId);
	//channel wasnt found
	if (foundIt == implementation->mChannels.end())
		return;

	//set FMOD vector position
	FMOD_VECTOR position = VectorToFmod(pos);
	//attempt to set attributes
	Audio::ErrorCheck(foundIt->second->set3DAttributes(&position, NULL));
}

void Audio::SetChannelVolume(int nChannelId, float volume)
{
	//attempt to find channel
	auto foundIt = implementation->mChannels.find(nChannelId);
	//if channel wasnt found, exit
	if (foundIt == implementation->mChannels.end())
		return;

	//attempt to set channel volume.
	Audio::ErrorCheck(foundIt->second->setVolume(dbToVolume(volume)));
}

bool Audio::IsEventPlaying(const string & strEventName) const
{
	//attempt to find event
	auto foundIt = implementation->mEvents.find(strEventName);
	//check if event was found, if not exit
	if (foundIt == implementation->mEvents.end())
		return false;

	//get the playback state
	FMOD_STUDIO_PLAYBACK_STATE* state = NULL;
	//check if it is playing
	if (foundIt->second->getPlaybackState(state) == FMOD_STUDIO_PLAYBACK_PLAYING)
		return true;


	return false;
}


//convert from db to volume
float Audio::dbToVolume(float db)
{
	return powf(10.0f,0.05f * db);
}

//convert volume to db
float Audio::VolumeToDb(float volume)
{
	return 20.0f * log10f(volume);
}

//convert vec3 to FMOD Vector
FMOD_VECTOR Audio::VectorToFmod(const vec3 & pos)
{
	FMOD_VECTOR fVec = { pos.x, pos.y, pos.z };
	return fVec;
}


//Implementation constructor
Implementation::Implementation()
{
	//create the studio system
	mpStudioSystem = NULL;
	Audio::ErrorCheck(FMOD::Studio::System::create(&mpStudioSystem));
	//attempt to initialize it
	Audio::ErrorCheck(mpStudioSystem->initialize(32, FMOD_STUDIO_INIT_LIVEUPDATE, FMOD_INIT_PROFILE_ENABLE, NULL));
	//create the core system that enables access to the low level FMOD API
	mpSystem = NULL;
	Audio::ErrorCheck(mpStudioSystem->getCoreSystem(&mpSystem));

}

//Implementation destructor
Implementation::~Implementation()
{
	//unload and free up memory
	Audio::ErrorCheck(mpStudioSystem->unloadAll());
	Audio::ErrorCheck(mpStudioSystem->release());
}

void Implementation::Update()
{
	//create stopped channels vector that we store the stopped channels in to delete
	vector<ChannelMap::iterator> StoppedChannels;

	for (auto it = mChannels.begin(), itEnd = mChannels.end(); it != itEnd; it++)
	{
		//check if the channel is playing or not
		bool isPlaying = false;
		it->second->isPlaying(&isPlaying);
		if (!isPlaying)
		{
			//push onto the stopped channels
			StoppedChannels.push_back(it);
		}
	}
	for (auto& it : StoppedChannels)
	{
		//remove stopped channel from map.
		mChannels.erase(it);
	}
	//attempt to update the audio system.
	Audio::ErrorCheck(mpStudioSystem->update());

}



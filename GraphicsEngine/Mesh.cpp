#include "Mesh.h"



Mesh::~Mesh()
{
	//close buffers
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ibo);
}

void Mesh::intialise(unsigned int vertexCount, const Vertex * vertices, unsigned int indexCount, unsigned int * indicies)
{
	assert(vao == 0);

	//generate buffers
	glGenBuffers(1, &vbo);
	glGenVertexArrays(1, &vao);

	//bind vertex array aka a mesh wrapper
	glBindVertexArray(vao);

	//bind vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	//fill vertex buffer
	glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(Vertex), vertices, GL_STATIC_DRAW);

	//enable firt element as position
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

	//enable the normal
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(Vertex), (void*)16);

	glEnableVertexAttribArray(2);
	//first 2 is the position in the vertex structure, second 2 is the number floats (so vec2 = 2 floats), GL_FLOAT = datatype, GL_FALSE = not normalized, sizeof skip size of one vertex,
	// 32 = number of bytes in (each float is 4 bytes, 8 * 4 = 32).
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)32);

	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)40);

	//there are indexs
	if (indexCount != 0)
	{
		//start index buffer
		glGenBuffers(1, &ibo);

		//bind vertex buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

		//fill vertex buffer
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			indexCount * sizeof(unsigned int), indicies, GL_STATIC_DRAW);

		triCount = indexCount / 3;
	}
	else
	{
		triCount = vertexCount / 3;
	}

	//unbind buffers
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

}

void Mesh::intialiseQuad()
{
	assert(vao == 0);

	//generate buffers
	glGenBuffers(1, &vbo);
	glGenVertexArrays(1, &vao);

	//bind vertex array aka a mesh wrapper
	glBindVertexArray(vao);

	//bind vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	//define the data for the 2 triangles that will make a quad.
	Vertex verticies[6];
	verticies[0].position = { -0.5f,0,0.5f,1 };
	verticies[1].position = { 0.5f,0,0.5f,1 };
	verticies[2].position = { -0.5f,0,-0.5f,1 };

	verticies[3].position = { -0.5f,0,-0.5f,1 };
	verticies[4].position = { 0.5f,0,0.5f,1 };
	verticies[5].position = { 0.5f,0,-0.5f,1 };

	verticies[0].texCoord = { 0,1 }; //BOTTOM LEFT
	verticies[1].texCoord = { 1,1 }; //BOTTOM RIGHT
	verticies[2].texCoord = { 0,0 }; //TOP LEFT
				
	verticies[3].texCoord = { 0,0 }; //TOP LEFT
	verticies[4].texCoord = { 1,1 }; //BTTOM RIGHT
	verticies[5].texCoord = { 1, 0 }; //TOP RIGHT

	//PLANE IS FACING UP
	verticies[0].normal = {0,1,0,0};
	verticies[1].normal = {0,1,0,0};
	verticies[2].normal = {0,1,0,0};
	verticies[3].normal = {0,1,0,0};
	verticies[4].normal = {0,1,0,0};
	verticies[5].normal = {0,1,0,0};


	//fill the vertex buffer.
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(Vertex), verticies, GL_STATIC_DRAW);

	//enable first element as position
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);

	//enable the normal
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, sizeof(Vertex), (void*)16);

	glEnableVertexAttribArray(2);
	//first 2 is the position in the vertex structure, second 2 is the number floats (so vec2 = 2 floats), GL_FLOAT = datatype, GL_FALSE = not normalized, sizeof skip size of one vertex,
	// 32 = number of bytes in (each float is 4 bytes, 8 * 4 = 32).
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)32);

	

	//unbind buffers
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//quad has two triangles
	triCount = 2;


}

void Mesh::intialiseFullScreenQuad()
{
	assert(vao == 0);
	//generate buffers
	glGenBuffers(1, &vbo);
	glGenVertexArrays(1, &vao);

	//bind vertex array aka a mesh wrapper
	glBindVertexArray(vao);

	//bind vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	float verticies[] = {
		-1,1, //top left
		-1,-1, // bottom left
		1,1, //right top
		-1,-1, //bottom left
		1,-1, //bottom right
		1,1, //top right
	};

	glBufferData(GL_ARRAY_BUFFER, 12 * sizeof(float), verticies, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 8, 0);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	triCount = 2;
}

//Find the min vertex based on the coordinate type desired
float Mesh::FindMin(vector<Mesh::Vertex*> verticies, string type)
{
	float min = INFINITE;
	for (unsigned i = 0; i < verticies.size(); i++)
	{
		//find the min x posiion
		if (type == "x")
		{
			if (verticies[i]->position.x < min)
			{
				min = verticies[i]->position.x;
			}
		}
		//find the min z posiion
		if (type == "z")
		{
			if (verticies[i]->position.z < min)
			{
				min = verticies[i]->position.z;
			}
		}
	}
	return min;
}

//Find the max vertex based on the coordinate type desired
float Mesh::FindMax(vector<Mesh::Vertex*> verticies, string type)
{
	float max = 0;
	for (unsigned i = 0; i < verticies.size(); i++)
	{
		//find the max x position
		if (type == "x")
		{
			if (verticies[i]->position.x > max)
			{
				max = verticies[i]->position.x;
			}
		}
		//find the max z position
		if (type == "z")
		{
			if (verticies[i]->position.z > max)
			{
				max = verticies[i]->position.z;
			}
		}
	}
	return max;
}

//generate tex coords for a vertex if no tex coords are assigned.
vec2 Mesh::GenerateTexCoord(vec3 vertexPosition, float absX, float absZ, float offSetX, float offSetZ)
{
	//Calculate coords
	float tS = vertexPosition.x + offSetX;
	tS = tS / absX;
	float tT = vertexPosition.z + offSetZ;
	tT = tT / absZ;

	return vec2(tS,tT);
}

void Mesh::CreateCube()
{
	//Assign box verticies and tex coords
	Mesh::Vertex boxVerticies[36];
	boxVerticies[0].position = { -0.5f, -0.5f, -0.5f,1 };
	boxVerticies[1].position = { 0.5f, -0.5f, -0.5f,1 };
	boxVerticies[2].position = { 0.5f,  0.5f, -0.5f,1 };
	boxVerticies[3].position = { 0.5f,  0.5f, -0.5f,1 };
	boxVerticies[4].position = { -0.5f, 0.5f, -0.5f,1 };
	boxVerticies[5].position = { -0.5f, -0.5f,-0.5f,1 };
	boxVerticies[6].position = { -0.5f, -0.5f, 0.5f,1 };
	boxVerticies[7].position = { 0.5f, -0.5f, 0.5f ,1 };
	boxVerticies[8].position = { 0.5f, 0.5f, 0.5f  ,1 };
	boxVerticies[9].position = { 0.5f, 0.5f, 0.5f  ,1 };
	boxVerticies[10].position = { -0.5f, 0.5f, 0.5f  ,1 };
	boxVerticies[11].position = { -0.5f, -0.5f, 0.5f ,1 };
	boxVerticies[12].position = { -0.5f, 0.5f, 0.5f  ,1 };
	boxVerticies[13].position = { -0.5f, 0.5f, -0.5f ,1 };
	boxVerticies[14].position = { -0.5f, -0.5f, -0.5f,1 };
	boxVerticies[15].position = { -0.5f, -0.5f, -0.5f,1 };
	boxVerticies[16].position = { -0.5f, -0.5f, 0.5f ,1 };
	boxVerticies[17].position = { -0.5f, 0.5f, 0.5f  ,1 };
	boxVerticies[18].position = { 0.5f, 0.5f, 0.5f   ,1 };
	boxVerticies[19].position = { 0.5f, 0.5f, -0.5f  ,1 };
	boxVerticies[20].position = { 0.5f, -0.5f, -0.5f ,1 };
	boxVerticies[21].position = { 0.5f, -0.5f, -0.5f ,1 };
	boxVerticies[22].position = { 0.5f, -0.5f, 0.5f  ,1 };
	boxVerticies[23].position = { 0.5f, 0.5f, 0.5f   ,1 };
	boxVerticies[24].position = { -0.5f, -0.5f, -0.5f,1 };
	boxVerticies[25].position = { 0.5f, -0.5f, -0.5f ,1 };
	boxVerticies[26].position = { 0.5f, -0.5f, 0.5f  ,1 };
	boxVerticies[27].position = { 0.5f, -0.5f, 0.5f  ,1 };
	boxVerticies[28].position = { -0.5f, -0.5f, 0.5f ,1 };
	boxVerticies[29].position = { -0.5f, -0.5f, -0.5f,1 };
	boxVerticies[30].position = { -0.5f, 0.5f, -0.5f ,1 };
	boxVerticies[31].position = { 0.5f, 0.5f, -0.5f  ,1 };
	boxVerticies[32].position = { 0.5f, 0.5f, 0.5f   ,1 };
	boxVerticies[33].position = { 0.5f, 0.5f, 0.5f   ,1 };
	boxVerticies[34].position = { -0.5f, 0.5f, 0.5f  ,1 };
	boxVerticies[35].position = { -0.5f, 0.5f, -0.5f,1 };
	boxVerticies[0].texCoord = { 0.0f, 0.0f };
	boxVerticies[1].texCoord = { 1.0f, 0.0f };
	boxVerticies[2].texCoord = { 1.0f, 1.0f };
	boxVerticies[3].texCoord = { 1.0f, 1.0f };
	boxVerticies[4].texCoord = { 0.0f, 1.0f };
	boxVerticies[5].texCoord = { 0.0f, 0.0f };
	boxVerticies[6].texCoord = { 0.0f, 0.0f };
	boxVerticies[7].texCoord = { 1.0f, 0.0f };
	boxVerticies[8].texCoord = { 1.0f, 1.0f };
	boxVerticies[9].texCoord = { 1.0f, 1.0f };
	boxVerticies[10].texCoord = { 0.0f, 1.0f };
	boxVerticies[11].texCoord = { 0.0f, 0.0f };
	boxVerticies[12].texCoord = { 1.0f, 0.0f };
	boxVerticies[13].texCoord = { 1.0f, 1.0f };
	boxVerticies[14].texCoord = { 0.0f, 1.0f };
	boxVerticies[15].texCoord = { 0.0f, 1.0f };
	boxVerticies[16].texCoord = { 0.0f, 0.0f };
	boxVerticies[17].texCoord = { 1.0f, 0.0f };
	boxVerticies[18].texCoord = { 1.0f, 0.0f };
	boxVerticies[19].texCoord = { 1.0f, 1.0f };
	boxVerticies[20].texCoord = { 0.0f, 1.0f };
	boxVerticies[21].texCoord = { 0.0f, 1.0f };
	boxVerticies[22].texCoord = { 0.0f, 0.0f };
	boxVerticies[23].texCoord = { 1.0f, 0.0f };
	boxVerticies[24].texCoord = { 0.0f, 1.0f };
	boxVerticies[25].texCoord = { 1.0f, 1.0f };
	boxVerticies[26].texCoord = { 1.0f, 0.0f };
	boxVerticies[27].texCoord = { 1.0f, 0.0f };
	boxVerticies[28].texCoord = { 0.0f, 0.0f };
	boxVerticies[29].texCoord = { 0.0f, 1.0f };
	boxVerticies[30].texCoord = { 0.0f, 1.0f };
	boxVerticies[31].texCoord = { 1.0f, 1.0f };
	boxVerticies[32].texCoord = { 1.0f, 0.0f };
	boxVerticies[33].texCoord = { 1.0f, 0.0f };
	boxVerticies[34].texCoord = { 0.0f, 0.0f };
	boxVerticies[35].texCoord = { 0.0f, 1.0f };

	//create the mesh
	intialise(36, boxVerticies);
}


//Used for terrains, can be any size you want
void Mesh::GenerateQuad(int rows, int columns)
{	
	vector<Mesh::Vertex*> quadVerticies;
	vector<unsigned int> indicies; //used for index buffer
	unsigned int vertIndex = 0;

	for (unsigned int r = 0; r < rows; r++)
	{
		for (unsigned int c = 0; c < columns; c++)
		{
			//essentially calculate a quad the same way as its done in the create quad function
			//as provided earlier, except apply an offset based on the current row and column
			Mesh::Vertex* vert1 = new Mesh::Vertex;

			vert1->position = { -0.5f + r, 0, 0.5f + c,1 };
			quadVerticies.push_back(vert1);
			indicies.push_back(vertIndex);	

			Mesh::Vertex* vert2 = new Mesh::Vertex;

			vert2->position = { 0.5f + r, 0, 0.5f + c,1 };
	

			quadVerticies.push_back(vert2);
			indicies.push_back(vertIndex + 1);

			Mesh::Vertex* vert3 = new Mesh::Vertex;

			vert3->position = { -0.5f + r, 0, -0.5f + c,1 };

			quadVerticies.push_back(vert3);
			indicies.push_back(vertIndex + 2);
			indicies.push_back(vertIndex + 2);

			Mesh::Vertex* vert4 = new Mesh::Vertex;

			vert4->position = { 0.5f + r, 0, -0.5f + c,1 };

			quadVerticies.push_back(vert4);
			indicies.push_back(vertIndex + 1);
			indicies.push_back(vertIndex + 3);	
			vertIndex += 4;
		}
	}


	//get min, max, offsets for x and z and absolute ranges required for calculating a tex coord
	float vMinX = FindMin(quadVerticies, "x");
	float vMaxX = FindMax(quadVerticies, "x");
	float absrangeX = (vMinX - vMaxX) * -1;
	float offsetX = 0 - vMinX;

	float vMinZ = FindMin(quadVerticies, "z");
	float vMaxZ = FindMax(quadVerticies, "z");
	float absrangeZ = (vMinZ - vMaxZ) * -1;
	float offsetZ = 0 - vMinZ;

	//pass the generated quads values and gem
	Mesh::Vertex* v = new Mesh::Vertex[quadVerticies.size()];
	unsigned int* in = new unsigned int[indicies.size()];

	for (unsigned int i = 0; i < quadVerticies.size(); i++)
	{
		v[i].position = quadVerticies[i]->position; //assign position
		v[i].normal = { 0,1,0,0 }; //assign normals to point upwards.
		v[i].texCoord = GenerateTexCoord(v[i].position,absrangeX,absrangeZ,offsetX,offsetZ); //calculate tex coords
	}

	//assign indexs for the index buffer
	for (unsigned int i = 0; i < indicies.size(); i++)
	{
		in[i] = indicies[i];
	}

	//create quad mesh
	intialise(quadVerticies.size(), v, indicies.size(), in);
}

void Mesh::draw()
{
	glBindVertexArray(vao);
	//using indicies or verticies
	if (ibo != 0)
	{
		glDrawElements(GL_TRIANGLES, 3 * triCount,
			GL_UNSIGNED_INT, 0);
	}
	else
	{
		glDrawArrays(GL_TRIANGLES, 0, 3 * triCount);
	}

}

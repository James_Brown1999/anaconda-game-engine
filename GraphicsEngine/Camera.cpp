#include "Camera.h"
#include <aie/Input.h>

//convert degrees to radians
const float deg2Rad = 3.14159f / 180.f;

//get projection view matrix based on application windows dimensions
mat4 Camera::GetProjectionMatrix(float w, float h)
{
	return perspective(pi<float>() * 0.25f,w/h, 0.1f, 1000.f);
}


mat4 Camera::GetViewMatrix()
{
	//convert theta to radians
	float thetaR = theta * deg2Rad;
	//convert phi to radians
	float phiR = phi * deg2Rad;
	//calculate forward vector
	forward = vec3(cos(phiR) * cos(thetaR), sin(phiR), cos(phiR)*sin(thetaR));

	//have camera look along its forward vector.
	return lookAt(position, position + forward, vec3(0,1,0));
}

vec3 Camera::getRight()
{
	//get the right vector by passing theta in radians to -sin and cos.
	return vec3(-sin(theta * deg2Rad), 0, cos(theta * deg2Rad));
}

vec3 Camera::getUp()
{
	//up vector is perpendicular to the right and forward. (cross product of the to)
	return cross(getRight(), getForward());
}

void Camera::Update(float deltaTime)
{
	aie::Input* input = aie::Input::getInstance();
	
	float mx = input->getMouseX();
	float my = input->getMouseY();

	//move forward if w is held
	if (input->isKeyDown(aie::INPUT_KEY_W))
	{
		position += getForward() * movementSpeed * deltaTime;
	}
	//move back if s is held
	if (input->isKeyDown(aie::INPUT_KEY_S))
	{
		position -= getForward() * movementSpeed * deltaTime;
	}
	//move right if d is held
	if (input->isKeyDown(aie::INPUT_KEY_D))
	{
		position += getRight() * movementSpeed * deltaTime;
	}
	//move left if a is held
	if (input->isKeyDown(aie::INPUT_KEY_A))
	{
		position -= getRight() * movementSpeed * deltaTime;
	}
	//speed up if shift is held
	if (input->isKeyDown(aie::INPUT_KEY_LEFT_SHIFT))
	{
		movementSpeed = acceleratedMovementSpeed;
	}
	else
	{
		movementSpeed = 20;
	}
	//if right mouse is held, rotate camera by changing values of theta and phi.
	if (input->isMouseButtonDown(aie::INPUT_MOUSE_BUTTON_RIGHT))
	{
		theta += sensitivity * (mx - lastMouseX);
		phi += sensitivity * (my - lastMouseY);
	}

	lastMouseX = mx;
	lastMouseY = my;


}

#include "Scene.h"

void Scene::Update(float deltaTime)
{
	if (m_postProcessingEnabled && m_intializeQuad && m_postProcessQuad == nullptr) //if post processing enabled, and the quad hasnt been created.
	{ 
		InitializePostProcessing(); //intialize.
	}
	if (!m_postProcessingEnabled && m_postProcessQuad != nullptr) //if post processing disabled and the quad exists
	{
		UnitializePostProcessing(); //unitalize and delete everything related to post processing.
	}


	for (auto object : gameObject)
	{
		if (object->tag == "SkyBox") //if the object is the skybox cube, set its position to the camera.
		{
			object->transform.position = camera.getPosition();
		}

		if (object->name == "Halo Needler") //if the halo needler, spin.
		{
			object->RotateY(deltaTime * 20);
		}

		object->update(deltaTime); //update gameObject
	}

	camera.Update(deltaTime); //update camera
}

void Scene::ClearScreen()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT); //clear the buffer
}

void Scene::Draw()
{
	
	//m_renderTarget.bind();
	if (m_postProcessingEnabled && !m_intializeQuad) //if post processing enabled, begin render target binding.
	{
		m_renderTarget->bind();
		ClearScreen();
	}

	DrawScene(); //draw the scene (All Meshs)
	
	//m_renderTarget.unbind();
	if (m_postProcessingEnabled && !m_intializeQuad) //if enabled, unbind render target and pass in uniforms.
	{
		m_renderTarget->unbind();
		ClearScreen();

		if (effectStatus[0]) //depth of field enabled
		{
			m_depthTarget->bind(); //begin depth target to grab the depth texture.
			ClearScreen();
			m_depthShader->bind(); //use shader to feed out depth values
			DrawScene(); //redraw scene
			m_depthTarget->unbind(); // unbind target
			ClearScreen();
		}

		//Bind the uniforms for the post processing
		m_postProcessingShader->bind();
		m_postProcessingShader->bindUniform("depthOfField", effectStatus[0]);
		m_postProcessingShader->bindUniform("boxBlur", effectStatus[1]);
		m_postProcessingShader->bindUniform("distortion", effectStatus[2]);
		m_postProcessingShader->bindUniform("edgeDetection", effectStatus[3]);
		m_postProcessingShader->bindUniform("grayScale", effectStatus[4]);
		m_postProcessingShader->bindUniform("sepia", effectStatus[5]);
		m_postProcessingShader->bindUniform("effectStrength", strengthEffect);
		m_postProcessingShader->bindUniform("colourTarget", 0);
		m_postProcessingShader->bindUniform("depthTarget", 1);
		m_postProcessingShader->bindUniform("lineThickness", 0.5f);
		m_postProcessingShader->bindUniform("focusNear", .5f);



		m_renderTarget->getTarget(0).bind(0); //insert textures into its slots on the graphics card.
		m_depthTarget->getTarget(0).bind(1);
		m_postProcessQuad->draw(); //draw quad mesh
	}
}

void Scene::DrawScene()
{
	vec4 white(1);
	vec4 black(0, 0, 0, 1);

	//draw grid
	for (int i = 0; i < 21; i++)
	{
		Gizmos::addLine(vec3(-10 + i, 0, 10),
			vec3(-10 + i, 0, -10),
			i == 10 ? white : black);
		Gizmos::addLine(vec3(10, 0, -10 + i),
			vec3(-10, 0, -10 + i),
			i == 10 ? white : black);
	}

	//visualizations for where the light source is located and its current colour.
	for (int i = 0; i < m_numLights; i++)
	{
		Gizmos::addSphere(m_lightPos[i], 1, 50, 50, vec4(m_lightColour[i], 1));
	}

	//draw gizmos
	Gizmos::draw(camera.GetProjectionMatrix(getScreenSize("Width"), getScreenSize("Height")) * camera.GetViewMatrix());

	//draw gameObjects
	for (int i = 0; i < gameObject.size(); i++)
	{
		gameObject[i]->Draw(this);
	}
}

void Scene::InitializePostProcessing()
{
	m_renderTarget = new RenderTarget();
	if (m_renderTarget->initialise(1, getScreenSize("Width"), getScreenSize("Height")) == false)  //intialize the render target
	{
		printf("Render Target Failed To Load");
		m_postProcessingEnabled = false;
		m_intializeQuad = false;
		return;
	}

	m_depthTarget = new RenderTarget();
	if (m_depthTarget->initialise(1, getScreenSize("Width"), getScreenSize("Height")) == false) //initialzie the depth target
	{
		printf("Render Target Failed To Load");
		m_postProcessingEnabled = false;
		m_intializeQuad = false;
		return;
	}

	m_depthShader = new ShaderProgram();
	m_depthShader->loadShader(eShaderStage::VERTEX, "../Assets/Shaders/normalmap.vert"); //bind the depth shader frag to the normap map vert
	m_depthShader->loadShader(eShaderStage::FRAGMENT, "../Assets/Shaders/Depth.frag"); //will ouput the normal map texture as a depth texture

	if (m_depthShader->link() == false) //link them
	{
		printf(m_depthShader->getLastError());
		m_postProcessingEnabled = false;
		return;
	}

	m_postProcessingShader = new ShaderProgram(); //create the post processing shader
	m_postProcessingShader->loadShader(eShaderStage::VERTEX, "../Assets/Shaders/PostProcess.vert");
	m_postProcessingShader->loadShader(eShaderStage::FRAGMENT, "../Assets/Shaders/PostProcess.frag");

	if (m_postProcessingShader->link() == false) //link them
	{
		printf(m_postProcessingShader->getLastError());
		m_postProcessingEnabled = false;
		return;
	}

	m_postProcessQuad = new Mesh(); //create the full screen quad
	m_postProcessQuad->intialiseFullScreenQuad();
	m_intializeQuad = false;
}

void Scene::UnitializePostProcessing()
{
	//delete and free up the memory
	delete m_postProcessQuad;
	delete m_renderTarget;
	delete m_depthTarget;
	delete m_depthShader;
	delete m_postProcessingShader;
	m_postProcessQuad = nullptr;
	m_renderTarget = nullptr;
	m_depthTarget = nullptr;
	m_depthShader = nullptr;
	m_postProcessingShader = nullptr;
	
}


void Scene::BindingUniforms(ShaderProgram * shader, GameObject* object)
{
	//bind the game object uniforms to its shader (Needs to be refactored).
	auto pvm = camera.GetProjectionMatrix(getScreenSize("Width"), getScreenSize("Height")) * camera.GetViewMatrix() * object->getTransform();
	shader->bindUniform("ProjectionViewModel", pvm);
	shader->bindUniform("Ia", m_ambientLight);
	shader->bindUniform("Id", m_diffuse);
	shader->bindUniform("Is", m_specular);
	shader->bindUniform("LightDirection", m_lightDir);
	shader->bindUniform("ModelMatrix", object->getTransform());
	shader->bindUniform("NormalMatrix", inverseTranspose(mat3(object->getTransform())));
	shader->bindUniform("cameraPosition", camera.getPosition());
	shader->bindUniform("roughness", object->getRoughness());
	shader->bindUniform("reflectionCoefficient", object->getReflectionCoefficient());
	shader->bindUniform("numLights", m_numLights);
	shader->bindUniform("lightPos", m_numLights, &m_lightPos[0]);
	shader->bindUniform("lightColour", m_numLights, &m_lightColour[0]);
	shader->bindUniform("lightPower", m_numLights, &m_lightPower[0]);
	shader->bindUniform("diffuseTexture", 0);
	shader->bindUniform("noiseTexture", 0);
	if (object->tag == "Terrain") //if terrain, bind terrain textures
	{
		shader->bindUniform("grassTexture", 10);
		shader->bindUniform("rockTexture", 11);
	}
	shader->bindUniform("noiseScale", (float)2);
	shader->bindUniform("colour", object->colour);


	if (object->tag == "SkyBox") //if cube map, bind cubemap texture
	{
		glBindTexture(GL_TEXTURE_CUBE_MAP, object->cubemap);
	}


}

int Scene::getScreenSize(string HeightorWidth)
{
	int w; int h;
	//Get Application window size
	glfwGetWindowSize(m_window, &w, &h);

	//return width
	if (HeightorWidth == "Width")
	{
		return w;
	}
	//return height
	if (HeightorWidth == "Height")
	{
		return h;
	}

	return 0;
}

//search every gameobject in scene until desired tag matches an objects tag.
GameObject* Scene::FindByTag(string tag)
{
	for (auto object : gameObject)
	{
		if (object->tag == tag)
		{
			return object;
		}
	}

	return nullptr;
}

//search every gameobject in scene until desired object name matches an objects name.
GameObject* Scene::Find(string name)
{
	for (auto object : gameObject)
	{
		if (object->name == name)
		{
			return object;
		}
	}
	return nullptr;
}



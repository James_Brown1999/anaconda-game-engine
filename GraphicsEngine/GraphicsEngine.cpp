// GraphicsEngine.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "Application.h"

using glm::vec3;
using glm::vec4;
using glm::mat4;
using namespace glm;
using namespace aie;

int main()
{
	Application* app = new Application();
	//Application* Application = new Application();
	app->run();
	//auto major = ogl_GetMajorVersion();
	//auto minor = ogl_GetMinorVersion();
	//printf("GL: %i.%i\n", major, minor);
	return 0;
}

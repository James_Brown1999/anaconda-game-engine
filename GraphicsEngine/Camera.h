#pragma once
#include <glm/glm/glm.hpp>
#include<glm/glm/ext.hpp>


using namespace glm;

class Camera
{
public:
	Camera() : theta(0), phi(-20), position(-10,4,0) {}
	~Camera() {}


	//get the cameras position
	vec3 getPosition() { return position; }
	//get its projection matrix
	mat4 GetProjectionMatrix(float w, float h);
	//get its view matrix
	mat4 GetViewMatrix();
	//get its forward vector
	vec3 getForward() { return forward; };
	//get its right vector
	vec3 getRight();
	//get its up vector
	vec3 getUp();
	//called evert frame
	void Update(float deltaTime);


protected:
	float theta;
	float phi;
	vec3 position;
	vec3 forward;
	vec3 up;
	int lastMouseX;
	int lastMouseY;
	float sensitivity = 1; //aim sensitivity
	float movementSpeed = 20; //standard movement speed
	float acceleratedMovementSpeed = 550; //fast movement speed (when holding shift)
};


#pragma once
#include <fmod_studio.h>
#include <fmod_studio.hpp>
#include <fmod.hpp>
#include <map>
#include <vector>
#include <math.h>
#include <iostream>
#include <string>
#include "Debug.h"
#include <glm/glm/glm.hpp>
#include<glm/glm/ext.hpp>
//using namespace irrklang;
using namespace std;
using namespace glm;

//collection of the data required for implementing the audio source
struct Implementation {
	Implementation();
	~Implementation();

	//ran every frame. (if audio source is active)
	void Update();

	//the studio system
	FMOD::Studio::System* mpStudioSystem;
	//the standard audio system
	FMOD::System* mpSystem;


	//the next channels ID
	int mnNextChannelId;
	int channelCount;

	//map for loaded sounds
	typedef map<string, FMOD::Sound*> SoundMap;
	//map for loaded channels
	typedef map<int, FMOD::Channel*> ChannelMap;
	//map for created channels
	typedef map<string, FMOD::Studio::EventInstance*> EventMap;
	//map for banks
	typedef map<string, FMOD::Studio::Bank*> BankMap;

	
	BankMap mBanks;
	EventMap mEvents;
	SoundMap mSounds;
	ChannelMap mChannels;
};


class Audio
{
public:

	//intialize audio souce
	void Init();
	//ran every frame (if implemented)
	void Update();
	//when deleting audio source
	void ShutDown();
	//check if fmod binding returns success
	static int ErrorCheck(FMOD_RESULT result);

	//loading an audio bank
	void LoadBank(const string& strBankName, FMOD_STUDIO_LOAD_BANK_FLAGS flags);
	//loading an event
	void LoadEvent(const string& strEventName);
	//loading a sound by file
	void LoadSound(const string& strSoundName, bool _3d = true, bool _looping = false);
	//unload sound
	void UnloadSound(const string& strSoundName);
	//set the position of the listener
	void Set3dListenerPos(const vec3& pos = { 0,0,0 }, const vec3& forward = { 0,0,0 }, const vec3& up = { 0,0,0 }, float volume = 0.0f);
	//play sound
	int PlaySound(const string& strSoundName, const vec3& pos = { 0,0,0 }, float volume = 0.0f);
	//play event
	void PlayEvent(const string& strEventName);
	//stop event immediatly or let it fade out
	void StopEvent(const string& strEventName, bool immediate = false);
	//set channels 3d position
	void SetChannel3dPosition(int nChannelId, const vec3& pos);
	//set the volume of a channel
	void SetChannelVolume(int nChannelId, float volume);
	//checks if an event is playing
	bool IsEventPlaying(const string& strEventName) const;
	//converts decibals to volume
	float dbToVolume(float db);
	//converts volume to decibals
	float VolumeToDb(float volume);
	//convert a standard vector to an FMOD vector
	FMOD_VECTOR VectorToFmod(const vec3& pos);

	//the implementation of the audio source.
	Implementation* implementation = nullptr;


	Audio();
	~Audio();
};


#pragma once
#include <glm/glm/glm.hpp>
#include<glm/glm/ext.hpp>
#include "gl_core_4_4.h"
#include <map>
#include <vector>

using namespace glm;
using namespace std;

class Mesh
{
public:
	Mesh() : triCount(0), vao(0), vbo(0), ibo(0) {} //assign default vertex, tri and indice count to 0.
	virtual ~Mesh();

	//collection of the vertex's data
	struct  Vertex
	{
		vec4 position;
		vec4 normal;
		vec2 texCoord;
	};

	//create the mesh
	void intialise(unsigned int vertexCount, const Vertex* vertices,
	unsigned int indexCount = 0,
	unsigned int* indicies = nullptr);
	//creating a basic fixed size quad
	void intialiseQuad();
	//create a quad to draw over the screen in order to display post processing effects
	void intialiseFullScreenQuad();
	//used for finding the min vertex position of a desired coord type (i.e. x or z)
	float FindMin(vector<Mesh::Vertex*> verticies, string type);
	//used for finding the max vertex position of a desired coord type (i.e. x or z)
	float FindMax(vector<Mesh::Vertex*> verticies, string type);
	//Generate a tex coord
	vec2 GenerateTexCoord(vec3 vertexPosition, float absX, float absZ, float offSetX, float offSetZ);
	//create a basic primitive cube
	void CreateCube();

	//create a quad of desired size, incredibly handy for terrain
	void GenerateQuad(int row, int columns);
	//draw the mesh.
	virtual void draw();


protected:

	unsigned int triCount;
	unsigned int vao, vbo, ibo;
};


#pragma once
#include <glm/glm/glm.hpp>
#include <glm/glm/mat4x4.hpp>
#include "gl_core_4_4.h"
#include<glm/glm/ext.hpp>
#include "Particles.h"
#include "Debug.h"
using namespace glm;
using namespace std;

class ParticleEmitter
{
public:
	ParticleEmitter() : m_particles(nullptr), m_firstDead(0), m_maxParticles(0), m_position(0,0,0),
		m_vao(0), m_vbo(0), m_ibo(0), m_vertexData(nullptr) {}
	~ParticleEmitter()
	{
		delete[] m_particles;
		delete[] m_vertexData;

		glDeleteVertexArrays(1, &m_vao);
		glDeleteBuffers(1, &m_vbo);
		glDeleteBuffers(1, &m_ibo);

	}


	void initialise(unsigned int maxParticles, unsigned int emitRate,
		float minLifeTime, float maxLifeTime,
		float minVelocity, float maxVelocity,
		float startSize, float endSize,
		const vec4& startColour, const vec4& endColour);

	void emit();

	void update(float deltaTime, const mat4& cameraTransform);

	void draw();

	
protected:

	Particles::Particle* m_particles;
	unsigned int m_firstDead;
	unsigned int m_maxParticles;

	unsigned int m_vao, m_vbo, m_ibo;
	Particles::ParticleVertex* m_vertexData;

	vec3 m_position;

	float m_emitTimer;
	float m_emitRate;

	float m_lifespanMin;
	float m_lifespanMax;

	float m_velocityMin;
	float m_velocityMax;

	float m_startSize;
	float m_endSize;

	vec4 m_startColour;
	vec4 m_endColour;

	

};


#pragma once
#include "pch.h"
#include <iostream>
#include "gl_core_4_4.h"
#include <GLFW/glfw3.h>
#include <aie/Gizmos.h>
#include <glm/glm/glm.hpp>
#include<glm/glm/ext.hpp>
#include "Camera.h"
#include "Shader.h"
#include "Mesh.h"
#include <glm/glm/mat4x4.hpp>
#include "OBJMesh.h"
#include <time.h>
#include "RenderTarget.h"
#include "GameObject.h"
#include <imgui.h>
#include "imgui_glfw3.h"
#include "ParticleEmitter.h"



using glm::vec3;
using glm::vec4;
using glm::mat4;
using namespace glm;
using namespace aie;
using namespace std;

class Application
{
public:
	Application();
	~Application();

	bool StartUp();
	bool Update();
	bool Draw();
	bool shutDown();
	void run();
	void clearScreen();

	bool ObjectTransformUI(bool active, static int i);
	bool LightTransformUI(bool active, static int i);
	void DrawHierarchy();
	void PostProcessingUI();

	

	

protected:
	bool m_startUp = true; //wanna be true from the get go as its needs to call functions and set variables when the Application opens.
	bool m_startUpComplete = false; //enables update of the program to commence.
	bool m_shutDown = false;
	mat4 m_view;
	mat4 m_projection;
	GLFWwindow* m_window;
	mat4 m_viewMatrix;
	mat4 m_projectionMatrix;
	Camera m_camera;
	float deltaTime = 0;
	float oldTime = 0;
	float newTime = 0;
	float focus = 0;
	GameObject* gameObject[4];

	ShaderProgram m_PhongShader;
	Mesh m_quadMesh;
	mat4 m_quadTransform;

	Mesh fullScreenQuad;
	ShaderProgram m_postProcessingShader;

	ShaderProgram waterShader;

	OBJMesh meesh;

	ShaderProgram m_depthShader;

	Texture m_quadTexture;
	//Texture m_texture;
	ShaderProgram m_texturedShader;

	ShaderProgram m_normalShader;

	RenderTarget m_renderTarget;
	RenderTarget m_depthRenderTarget;
	ShaderProgram particleShader;


	Scene* m_scene;

	struct Light {
		vec3 direction;
		vec3 diffuse;
		vec3 specular;
	};
	
	Light m_light;
	vec3 m_ambientLight;
};
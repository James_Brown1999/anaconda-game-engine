#pragma once
#include <glm/glm/glm.hpp>
#include <glm/glm/mat4x4.hpp>
#include<glm/glm/ext.hpp>
using namespace glm;
using namespace std;

class Particles
{
public:

	struct Particle
	{
		vec3 position;
		vec3 velocity;
		vec4 colour;
		float size;
		float lifetime;
		float lifespan;
	};

	struct ParticleVertex
	{
		vec4 position;
		vec4 colour;
	};

	Particles();
	~Particles();
};


#pragma once
#include <glm/glm/glm.hpp>
#include <stb/stb_image.h>
#include<glm/glm/ext.hpp>
#include "Camera.h"
#include "Shader.h"
#include "Mesh.h"
#include <glm/glm/mat4x4.hpp>
#include "OBJMesh.h"
#include <string>
#include "Scene.h"
#include "Texture.h"
#include "ParticleEmitter.h"
#include "Audio.h"


class Scene;
using namespace glm;
using namespace aie;
using namespace std;

//transform data container. Allows it to look like Unity :)
struct Transform {
	vec3 position;
	vec3 scale;
	vec3 eulerAngle;
};


class GameObject
{
public:
	GameObject();
	//when creating a new object provide a model file(optional), texture file (optional), rotation, a scale, a colour, roughness, set how shint it is, its name and tag
	GameObject(const char* modelFile, const char* textureFile ,vec3 position, vec3 rotation,vec3 scale, vec3 col,float roughness, float reflectionCoefficient, string objName, string objTag);
	~GameObject();

	//its transform
	Transform transform;
	vec3 colour; //is colour
	//rotate it around the x axis
	void RotateX(float radians);
	//rotate it around the y axis
	void RotateY(float radians);
	//rotate it around the z axis
	void RotateZ(float radians);
	//rotate around all axis 
	void Rotate(float x, float y, float z);
	//get the current transform matrix
	mat4 getTransform();
	//get the current attached shader
	ShaderProgram* getShader() { return m_shader; }
	//set the current shader
	ShaderProgram* setShader(ShaderProgram* shader);
	//called on start up.
	bool start(const char* vertexShader, const char* fragShader, bool cube);
	//called every frame
	bool update(float deltaTime);
	//called every frame
	void Draw(Scene* scene);
	//get the roughness
	float getRoughness() { return m_roughness; }
	//get the shinyness
	float getReflectionCoefficient() { return m_reflectionCoefficient; }
	string tag = "";
	string name = "Untitled";
	//used for skybox, assign a texture
	unsigned int CreateCubeMap(vector<string> faces);

	//textures for cube map
	vector<string> faces
	{
		"../Assets/Textures/left.jpg",
		"../Assets/Textures/right.png",
		"../Assets/Textures/top.png",
		"../Assets/Textures/bottom.png",
		"../Assets/Textures/front.jpg",
		"../Assets/Textures/back.jpg"
		
	};

	//used for generating terrain noise texture.
	unsigned char* GenerateTerrain();

	//get the mesh of the object
	Mesh* GetMesh() { return m_mesh; }

	
	unsigned int cubemap = CreateCubeMap(faces);
	//the grass texture that covers the black elements of the noise texture
	Texture* terrainDefaultTexture;
	//the rock texture that covers the red elements of the noise texture
	Texture* terrainHeightTexture;
	//the scene its in
	Scene* currentScene;



protected:
	//its shader
	ShaderProgram* m_shader;
	//its loaded mesh (of .obj type)
	OBJMesh* m_objmesh;
	//its texture
	Texture* m_texture;
	//its generate primitive mesh (if no obj mesh is loaded)
	Mesh* m_mesh;
	
	//an audio source
	Audio* audioSource;

	//asset file location
	const char* m_meshName;
	//texture asset file location
	const char* m_textureName;
	float m_roughness = 0;
	float m_reflectionCoefficient = 0;
};


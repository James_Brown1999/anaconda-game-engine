#include "GameObject.h"


GameObject::GameObject()
{
}

GameObject::GameObject(const char* modelFile, const char* textureFile, vec3 position, vec3 rotation, vec3 scale, vec3 col, float roughness, float reflectionCoefficient, string objName, string objTag)
{
	//pass in objects mesh, transforms and light repsonse properties
	m_meshName = modelFile;
	m_textureName = textureFile;
	transform.position = position;
	transform.eulerAngle = rotation;
	transform.scale = scale;
	colour = col;
	m_roughness = roughness;
	m_reflectionCoefficient = reflectionCoefficient;
	name = objName;
	tag = objTag;
}


GameObject::~GameObject()
{
}

void GameObject::RotateX(float radians)
{
	//rotate x
	transform.eulerAngle.x += radians;
}
void GameObject::RotateY(float radians)
{
	//rotate y
	transform.eulerAngle.y += radians;
}
void GameObject::RotateZ(float radians)
{
	//rotate z
	transform.eulerAngle.z += radians;
}
void GameObject::Rotate(float x, float y, float z)
{
	//rotate all axis
	RotateX(x);
	RotateY(y);
	RotateZ(z);
}

mat4 GameObject::getTransform()
{
	//transform matrix
	float radians = 6.28f / 360.0f;
	mat4 tranformMatrix = translate(mat4(1), transform.position)
		* rotate(mat4(1), transform.eulerAngle.z * radians, vec3(0, 0, 1))
		* rotate(mat4(1), transform.eulerAngle.y * radians, vec3(0, 1, 0))
		* rotate(mat4(1), transform.eulerAngle.x * radians, vec3(1, 0, 0))
		* scale(mat4(1), transform.scale);
	return tranformMatrix;
}

ShaderProgram * GameObject::setShader(ShaderProgram * shader)
{
	return m_shader = shader;	
}

bool GameObject::start(const char* vertexShader, const char* fragShader, bool cube)
{
	if (m_meshName != nullptr) //if the user is creating an object using a mesh, load from a file.
	{
		//create and load mesh
		m_objmesh = new OBJMesh();

		//load mesh and check if it fails
		if (m_objmesh->load(m_meshName, true, true) == false)
		{
			//error
			printf("Model Mesh Error!\n");
			return false;
		}
	}

	//create an audio source thats attached to needler with halo theme song playing
	if (name == "Halo Needler")
	{
		audioSource = new Audio();
		audioSource->LoadSound("../Assets/Audio/HaloTheme.wav",true,true);
		audioSource->PlaySound("../Assets/Audio/HaloTheme.wav", transform.position,0.2f);
	}

	//create an audio source thats attached to homer with the simpsons theme playing
	if (name == "Homer J Simpson")
	{
		audioSource = new Audio();
		audioSource->LoadSound("../Assets/Audio/SimpsonsTheme.wav", true, true);
		audioSource->PlaySound("../Assets/Audio/SimpsonsTheme.wav", transform.position, 0.2f);
	}

	//if a cube is being created
	if (cube)
	{
		//create cube mesh
		m_mesh = new Mesh();	
		m_mesh->CreateCube();
		//increase scale for skybox
		transform.scale *= 4;
	}

	if (!cube) //not a cube
	{
		if (m_textureName != nullptr) //if the user is loading a texture
		{
			//create and load texture, throw error if texture fails to load.
			m_texture = new Texture();
			if (m_texture->load(m_textureName) == false)
			{
				printf("Failed to load texture!\n");
			}
		}

		//no texture is provided but is a terrain object
		if (m_textureName == nullptr && tag == "Terrain")
		{
			//generate noise texutre
			m_texture = new Texture();
			m_texture->create(120, 120, aie::Texture::RED, GenerateTerrain());

			//create and load grass texture
			terrainDefaultTexture = new Texture();
			if (terrainDefaultTexture->load("../Assets/Textures/Grass.jpg") == false)
			{
				printf("Failed to load texture!\n");
			}
			//create and load rock texture
			terrainHeightTexture = new Texture();
			if (terrainHeightTexture->load("../Assets/Textures/Rock.jpg") == false)
			{
				printf("Failed to load texture!\n");
			}
			//generate terrain mesh
			m_mesh = new Mesh();
			m_mesh->GenerateQuad(120, 120);
		}
	}
	//load and link the shader
	m_shader->loadShader(eShaderStage::VERTEX, vertexShader);
	m_shader->loadShader(eShaderStage::FRAGMENT, fragShader);
	if (m_shader->link() == false)
	{
		printf(m_shader->getLastError());
		return false;
	}
	
	return true;
}

bool GameObject::update(float deltaTime)
{
	//if an audio source is attached, play it and set the sources listeners position
	if (audioSource != nullptr)
	{
		audioSource->SetChannel3dPosition(audioSource->implementation->channelCount, transform.position);
		audioSource->Set3dListenerPos(currentScene->camera.getPosition(), currentScene->camera.getForward(), -currentScene->camera.getUp());

		audioSource->Update();
	}

	return true;
}

void GameObject::Draw(Scene * scene)
{
	m_shader->bind(); //bind the shader
	scene->BindingUniforms(m_shader, this); //feed its uniforms to the shader


	if (m_texture != nullptr)
	{
		m_texture->bind(0);
	}

	if (m_objmesh != nullptr) //if the object has a loaded mesh draw it
	{
		m_objmesh->draw();
	}
	if (terrainDefaultTexture != nullptr) //has a terrain texture
	{
		terrainDefaultTexture->bind(10);
		terrainHeightTexture->bind(11);
	}
	if (m_mesh != nullptr) //has a generic mesh
	{	
		m_mesh->draw(); //draw mesh
	}
}

unsigned int GameObject::CreateCubeMap(vector<string> faces)
{
	unsigned int textureID;
	//generate texture
	glGenTextures(1, &textureID);
	//bind texture with a cubemap target
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
	
	 int width= 0, height = 0, nrChannels = 0;


	for (unsigned int i = 0; i < faces.size(); i++)
	{
		//load the face
		unsigned char *data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, STBI_default);

		if (data)
		{
			//passing in texture
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
				0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			//free the loaded image
			stbi_image_free(data);
		}
		else
		{
			//failed to load texture for cube map, throw error
			//string errorMessage = "CUBEMAP TEX FAILED TO LOAD AT PATH: " + faces[i];
			printf("CUBEMAP TEX FAILED TO LOAD AT PATH:");
			//free data
			stbi_image_free(data);
		}
	}

	//specify the cubemaps wrapping and filtering methods
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	//return the texture ID.
	return textureID;

}

//Generate terrain
unsigned char * GameObject::GenerateTerrain()
{
	int perlinDimensions = 120;
	unsigned char* noiseData = new unsigned char[perlinDimensions * perlinDimensions];

	//control the size of the Perlin noise
	float scale = (1.0f / perlinDimensions) * 3;

	//levels of detail (6 should give us rocky terrain whereas 1 could be mountains)
	int octaves = 6;

	for (int x = 0; x < perlinDimensions; x++)
	{
		for (int y = 0; y < perlinDimensions; y++)
		{
			//the amplitude and persistance of each step
			float amplitude = 9.f;
			float persistence = 0.3f;

			float perlinData = 0;

			for (int octave = 0; octave < octaves; octave++)
			{
				//lower the frequency each octave
				float freq = powf(2, (float)octave);

				//calc perlin and scale it.
				float perlinSample = perlin(vec2((float)x, (float)y) * scale * freq);

				//combine data with scaled amplitude
				perlinData += perlinSample * amplitude;

				//lower amplitude for next octave
				amplitude *= persistence;
			}
			//pass and store the generated noise data as a value between 0-255
			noiseData[x * perlinDimensions + y] = unsigned char((perlinData * 0.5f + 0.5f) * 255);
		}
	}

	return noiseData;
}

#include "ParticleEmitter.h"

void ParticleEmitter::initialise(unsigned int maxParticles, unsigned int emitRate,
	float minLifeTime, float maxLifeTime,
	float minVelocity, float maxVelocity,
	float startSize, float endSize,
	const vec4& startColour, const vec4& endColour)
{
	//set up emit timers
	m_emitTimer = 0;
	m_emitRate = 1.0f / emitRate;

	//store all variables passed in
	m_startColour = startColour;
	m_endColour = endColour;
	m_startSize = startSize;
	m_endSize = endSize;
	m_velocityMin = minVelocity;
	m_velocityMax = maxVelocity;
	m_lifespanMin = minLifeTime;
	m_lifespanMax = maxLifeTime;
	m_maxParticles = maxParticles;

	//create particle array
	m_particles = new Particles::Particle[m_maxParticles];


	for (int i = 0; i < m_maxParticles; i++)
	{
		m_particles[i].lifetime = m_lifespanMin;
		m_particles[i].lifespan = m_lifespanMax;

	}

	m_firstDead = 0;

	//create the array of verts for the particles
	//4 verts per particle for a quad
	//will be filled during update
	m_vertexData = new Particles::ParticleVertex[m_maxParticles * 4];

	//create the index buffer data for the particles
	//6 indicies per quad of two triangles
	//fill it now as it never changes
	unsigned int* indexData = new unsigned int[m_maxParticles * 6];
	for (unsigned int i = 0; i < m_maxParticles; i++)
	{
		indexData[i * 6 + 0] = i * 4 + 0;
		indexData[i * 6 + 1] = i * 4 + 1;
		indexData[i * 6 + 2] = i * 4 + 2;

		indexData[i * 6 + 3] = i * 4 + 0;
		indexData[i * 6 + 4] = i * 4 + 2;
		indexData[i * 6 + 5] = i * 4 + 3;
	}

	//create buffers
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ibo);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, m_maxParticles * 4 * sizeof(Particles::ParticleVertex), m_vertexData, GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_maxParticles * 6 * sizeof(unsigned int), indexData, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0); //position
	glEnableVertexAttribArray(1); //colour
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Particles::ParticleVertex), 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Particles::ParticleVertex), ((char*)0) + 16);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	delete[] indexData;

}

void ParticleEmitter::emit()
{
	if (m_firstDead >= m_maxParticles)
	{
		return;
	}

	Particles::Particle& particle = m_particles[m_firstDead++];

	//assign starting position
	particle.position = m_position;

	//randomise starting size and colour
	particle.colour = m_startColour;
	particle.size = m_startSize;

	//randomise velocity direction and strength
	float velocity = (rand() / (float)RAND_MAX) *
		(m_velocityMax - m_velocityMin) + m_velocityMin;
	particle.velocity.x = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity.y = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity.z = (rand() / (float)RAND_MAX) * 2 - 1;
	particle.velocity = normalize(particle.velocity) * velocity;




}

void ParticleEmitter::update(float deltaTime, const mat4 & cameraTransform)
{
	//spawn particles

	m_emitTimer += deltaTime;
	while (m_emitTimer > m_emitRate)
	{
		emit();
		m_emitTimer -= m_emitRate;
	}

	unsigned int quad = 0;

	for (unsigned int i = 0; i < m_firstDead; i++)
	{
		Particles::Particle* particle = &m_particles[i];

		particle->lifetime += deltaTime;
		if (particle->lifetime >= particle->lifespan)
		{
			*particle = m_particles[m_firstDead - 1];
			m_firstDead--;
		}
		else
		{
			particle->position += particle->velocity * deltaTime;

			particle->size = mix(m_startSize, m_endSize,
				particle->lifetime / particle->lifespan);

			particle->colour = mix(m_startColour, m_endColour,
				particle->lifetime / particle->lifespan);


			float halfSize = particle->size * 0.5f;

			m_vertexData[quad * 4 + 0].position = vec4(halfSize,
				halfSize, 0, 1);
			m_vertexData[quad * 4 + 0].colour = particle->colour;
			m_vertexData[quad * 4 + 1].position = vec4(-halfSize,
				halfSize, 0, 1);
			m_vertexData[quad * 4 + 1].colour = particle->colour;
			m_vertexData[quad * 4 + 2].position = vec4(-halfSize,
				-halfSize, 0, 1);
			m_vertexData[quad * 4 + 2].colour = particle->colour;
			m_vertexData[quad * 4 + 3].position = vec4(halfSize,
				-halfSize, 0, 1);
			m_vertexData[quad * 4 + 3].colour = particle->colour;


			vec3 zAxis = normalize(vec3(cameraTransform[3]) -
				particle->position);
			vec3 xAxis = cross(vec3(cameraTransform[1]), zAxis);
			vec3 yAxis = cross(xAxis, zAxis);

			mat4 billboard(vec4(xAxis, 0),
				vec4(yAxis, 0),
				vec4(zAxis, 0),
				vec4(0, 0, 0, 1));

			m_vertexData[quad * 4 + 0].position = billboard *
				m_vertexData[quad * 4 + 0].position +
				vec4(particle->position, 0);
			m_vertexData[quad * 4 + 1].position = billboard *
				m_vertexData[quad * 4 + 1].position +
				vec4(particle->position, 0);
			m_vertexData[quad * 4 + 2].position = billboard *
				m_vertexData[quad * 4 + 2].position +
				vec4(particle->position, 0);
			m_vertexData[quad * 4 + 3].position = billboard *
				m_vertexData[quad * 4 + 3].position +
				vec4(particle->position, 0);
			++quad;



		}
	}
}

void ParticleEmitter::draw()
{
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_firstDead * 4 * sizeof(Particles::ParticleVertex), m_vertexData);

	glBindVertexArray(m_vao);
	glDrawElements(GL_TRIANGLES, m_firstDead * 6, GL_UNSIGNED_INT, 0);
}

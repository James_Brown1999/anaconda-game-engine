#include "Application.h"
#include <aie/Input.h>

Application::Application()
{

}

Application::~Application()
{
}

bool Application::StartUp()
{

	if (glfwInit() == false)
	{
		return false;
	}
	m_window = glfwCreateWindow(1280, 720, "Graphics Engine", nullptr, nullptr);

	if (m_window == nullptr)
	{
		glfwTerminate();
		return false;
	}
	glfwMakeContextCurrent(m_window);

	if (ogl_LoadFunctions() == ogl_LOAD_FAILED)
	{
		glfwDestroyWindow(m_window);
		glfwTerminate();
		return false;
	}

	Gizmos::create(100000, 100000, 100000, 100000);

	/////////////////////Create the scene////////////////////////////
	m_scene = new Scene(vec3(10,0,10),vec3(0.15f, 0.25f, 0.25f), vec3(1,1,1), vec3(0.2f,0.2f,0.2f), m_window);
	//set light positions, colour and power.
	m_scene->setNumOfLights(4);

	m_scene->setLightPosition(vec3(10, 2, 0), 0);
	m_scene->setLightColour(vec3(0, 255, 0), 0);
	m_scene->setLightPower(10, 0);

	m_scene->setLightPosition(vec3(-10, 2, 0), 1);
	m_scene->setLightColour(vec3(255, 0, 0), 1);
	m_scene->setLightPower(10, 1);

	m_scene->setLightPosition(vec3(0, 2, 10), 2);
	m_scene->setLightColour(vec3(0, 0, 255), 2);
	m_scene->setLightPower(10,2);

	m_scene->setLightPosition(vec3(0, 2, -10), 3);
	m_scene->setLightColour(vec3(255, 127, 80), 3);
	m_scene->setLightPower(10, 3);
	//////////////////////////////////////////

	///////////////////////////Initialize Scene Objects////////////////////////////////////////////////
	gameObject[0] = new GameObject(nullptr, nullptr, vec3(-60, -1, -60), vec3(0, 0, 0), vec3(1), vec3(0, 0, 1), 0.2f, 0.2f, "Ground", "Terrain");
	gameObject[0]->currentScene = m_scene;
	gameObject[0]->setShader(&waterShader);
	gameObject[0]->start("../Assets/Shaders/Terrain.vert", "../Assets/Shaders/Terrain.frag", false);
	gameObject[1] = new GameObject("../Assets/Models/Needler.obj", "../Assets/Models/Needler_purple_color.tga", 
		vec3(-50, 5, -50), vec3(0, 0, 0), vec3(1), vec3(1,1,1),0.2f, 0.2f, "Halo Needler", "Weapon");
	gameObject[1]->currentScene = m_scene;
	gameObject[1]->setShader(&m_normalShader);
	gameObject[1]->start("../Assets/Shaders/normalmap.vert", "../Assets/Shaders/normalmap.frag", false);
	gameObject[2] = new GameObject(nullptr, nullptr, vec3(0, 0, 0), vec3(0, 0, 0), vec3(200, 200, 200), vec3(1, 1, 1),0, 0.2, "Sky", "SkyBox");
	gameObject[2]->currentScene = m_scene;
	gameObject[2]->setShader(&m_texturedShader);
	gameObject[2]->start("../Assets/Shaders/skybox.vert", "../Assets/Shaders/skybox.frag", true);
	gameObject[3] = new GameObject("../Assets/Models/card11_geoShapeh.obj", nullptr ,
		vec3(0, 10, 0), vec3(0, 0, 0), vec3(1), vec3(1, 1, 1), 0.2f, 0.2f, "BuzzCard", "Simpsons");
	gameObject[3]->currentScene = m_scene;
	gameObject[3]->setShader(&m_normalShader);
	gameObject[3]->start("../Assets/Shaders/normalmap.vert", "../Assets/Shaders/normalmap.frag", false);
	gameObject[4] = new GameObject("../Assets/Models/The Homer/TheHomer.obj", nullptr,
		vec3(47, 5, 50), vec3(0, 180, 0), vec3(1), vec3(1, 1, 1), 0.2f, 0.2f, "The Homer Car", "Simpsons");
	gameObject[4]->currentScene = m_scene;
	gameObject[4]->setShader(&m_normalShader);
	gameObject[4]->start("../Assets/Shaders/normalmap.vert", "../Assets/Shaders/normalmap.frag", false);
	gameObject[5] = new GameObject("../Assets/Models/Homer/Homer.obj", nullptr,
		vec3(50, 5, 50), vec3(-90, 0, 0), vec3(1), vec3(1, 1, 1), 0.2f, 0.2f, "Homer J Simpson", "Simpsons");
	gameObject[5]->currentScene = m_scene;
	gameObject[5]->setShader(&m_normalShader);
	gameObject[5]->start("../Assets/Shaders/normalmap.vert", "../Assets/Shaders/normalmap.frag", false);

	m_scene->gameObject.push_back(gameObject[0]);
	m_scene->gameObject.push_back(gameObject[1]);
	m_scene->gameObject.push_back(gameObject[2]);
	m_scene->gameObject.push_back(gameObject[3]);
	m_scene->gameObject.push_back(gameObject[4]);
	m_scene->gameObject.push_back(gameObject[5]);


	////////////////////////////////////////////////////////////////////////////////////////////////

	Input::create(); //create the input

	ImGui_Init(m_window, true); //create the UI in the current window.



	return true;
}

bool Application::Update()
{
	ImGui_NewFrame();
	Input::getInstance()->clearStatus();
	aie::Input* input = aie::Input::getInstance();
	glfwSetWindowSizeCallback(m_window, [](GLFWwindow*, int w, int h) { glViewport(0, 0, w, h); });

	//calculate deltaTime
	oldTime = newTime;
	newTime = (float)glfwGetTime();
	deltaTime = (newTime - oldTime) / 16;

	m_view = lookAt(vec3(10, 10, 10), vec3(0), vec3(0, 1, 0));
	m_projection = perspective(pi<float>() * 0.25f, 16 / 9.f, 0.1f, 1000.f);

	float time = glfwGetTime();
	m_light.direction = vec3(10, time, 10);


	m_scene->Update(deltaTime); //Update the scene
	//emitter->update(deltaTime, m_scene->camera.GetProjectionMatrix(m_scene->getScreenSize("Width"), m_scene->getScreenSize("Height")));
	
	cout << focus << endl;

	if (m_shutDown)
	{
		return false;
	}
	return true;
}

bool Application::Draw()
{
	glClearColor(0.25f, 0.25f, 0.25f, 1);
	glEnable(GL_DEPTH_TEST);

	while (glfwWindowShouldClose(m_window) == false &&
		glfwGetKey(m_window, GLFW_KEY_ESCAPE) != GLFW_PRESS)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		Gizmos::clear();
		static bool hierarchy = true; //hierarchy open
		static int selectedObject; //the currently selected object
		static int selectedLight; // the currently selected light
		static bool gameObjectTransform = true; //game object transform editor is open
		static bool lightTransform = false; //light source transform editor is open


		/////////Create the Hierarchy/////////////////////////
		ImGui::Begin("Scene Hierarchy", &hierarchy, ImGuiWindowFlags_AlwaysAutoResize);
		ImGui::Text("                      ");
		ImGui::SetWindowPos(ImVec2(0, 0));
		ImGui::Separator();
		ImGui::Columns(1, "sceneObjects");
		for (int i = 0; i < m_scene->gameObject.size(); i++)
		{
			char label[32];
			sprintf_s(label, m_scene->gameObject[i]->name.c_str(), i);
			if (ImGui::Selectable(label))
			{
				selectedObject = i;
				gameObjectTransform = true;
				lightTransform = false;
			}
		}
		ImGui::Separator();
		for (int i = 0; i < m_scene->getNumOfLights(); i++)
		{
			char label[32];
			sprintf_s(label, "Light %d", i);
			if (ImGui::Selectable(label))
			{
				selectedLight = i;
				gameObjectTransform = false;
				lightTransform = true;
			}
		}
		ImGui::End();

		
		ObjectTransformUI(gameObjectTransform, selectedObject); // update and draw the UI
		LightTransformUI(lightTransform, selectedLight);
		PostProcessingUI();
		
		//waterShader->bind();
		//waterShader->bindUniform("ProjectionViewModel", m_scene->camera.GetProjectionMatrix(m_scene->getScreenSize("Width"), m_scene->getScreenSize("Height")));

		//testMesh->draw();
		/*particleShader->bind();
		particleShader->bindUniform("ProjectionViewMatrix", m_scene->camera.GetProjectionMatrix(m_scene->getScreenSize("Width"), m_scene->getScreenSize("Height")));

		*/
		m_scene->Draw();
		//gameObject[0]->getShader()->bindUniform("", 0);

		


		ImGui::Render(); //render UI




		glfwSwapBuffers(m_window);

		glfwPollEvents();


		return true;
	}
	m_shutDown = true;
	return false;
}

bool Application::shutDown()
{
	
	Gizmos::destroy();
	glfwDestroyWindow(m_window);
	return true;
}

void Application::run()
{

	if (StartUp() == true)
	{
		while (Update() == true)
		{
			Draw();
		}
		shutDown();
	}
}

void Application::clearScreen()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

bool Application::ObjectTransformUI(bool active, static int i)
{
	static bool tranformWindow = true;

	if (active) //is UI active
	{
		//get the currently selected objects transform values
		float currentPosition[3] = { m_scene->selectedObject(i)->transform.position.x, m_scene->selectedObject(i)->transform.position.y, m_scene->selectedObject(i)->transform.position.z };
		float currentRotation[3] = { m_scene->selectedObject(i)->transform.eulerAngle.x, m_scene->selectedObject(i)->transform.eulerAngle.y, m_scene->selectedObject(i)->transform.eulerAngle.z };
		float currentScale[3] = { m_scene->selectedObject(i)->transform.scale.x, m_scene->selectedObject(i)->transform.scale.y, m_scene->selectedObject(i)->transform.scale.z };
		float currentColour[3] = { m_scene->selectedObject(i)->colour.x, m_scene->selectedObject(i)->colour.y, m_scene->selectedObject(i)->colour.z };


		ImGui::Begin("GameObject Transform", &tranformWindow, ImGuiWindowFlags_AlwaysAutoResize);
		ImGui::SetWindowPos(ImVec2(m_scene->getScreenSize("Width") - 255, 0));
		ImGui::Text("Position (x,y,z)         ");
		////If any input entered into the transform input boxes, update the transform.
		if (ImGui::InputFloat3("pos", currentPosition))
		{
			m_scene->selectedObject(i)->transform.position = vec3(currentPosition[0], currentPosition[1], currentPosition[2]);

		}
		ImGui::Separator();
		ImGui::Text("Rotation (x,y,z)         ");
		if (ImGui::InputFloat3("rot", currentRotation))
		{ 
			m_scene->selectedObject(i)->transform.eulerAngle = vec3(currentRotation[0], currentRotation[1], currentRotation[2]);
		}
		ImGui::Separator();
		ImGui::Text("Scale (x,y,z)            ");
		if (ImGui::InputFloat3("sca", currentScale))
		{
			m_scene->selectedObject(i)->transform.scale = vec3(currentScale[0], currentScale[1], currentScale[2]);
		}
		ImGui::Separator();
		ImGui::Text("Colour (r,g,b)                ");
		if (ImGui::InputFloat3("col", currentColour))
		{
			m_scene->selectedObject(i)->colour = vec3(currentColour[0], currentColour[1], currentColour[2]);
		}
		ImGui::End();
		return true;
	}

	return false;
}

bool Application::LightTransformUI(bool active, static int i)
{
	static bool transformWindow = true;
	if (active) //Is UI active
	{
		//get the currently selected lights transform values
		float currentPosition[3] = { m_scene->getLightPosition(i).x, m_scene->getLightPosition(i).y, m_scene->getLightPosition(i).z };
		float currentColour[3] = { m_scene->getLightColour(i).x, m_scene->getLightColour(i).y, m_scene->getLightColour(i).z };
		float currentPower = m_scene->getLightPower(i);

		ImGui::Begin("Light Transform", &transformWindow, ImGuiWindowFlags_AlwaysAutoResize);
		ImGui::SetWindowPos(ImVec2(m_scene->getScreenSize("Width") - 255, 0));
		ImGui::Text("Position (x,y,z)         ");
		////If any input entered into the transform input boxes, update the transform.
		if (ImGui::InputFloat3("pos", currentPosition))
		{
			 m_scene->setLightPosition(vec3(currentPosition[0], currentPosition[1], currentPosition[2]),i);
		}
		ImGui::Separator();
		ImGui::Text("Colour (r,g,b)         ");
		if (ImGui::InputFloat3("col", currentColour))
		{
			m_scene->setLightColour(vec3(currentColour[0], currentColour[1], currentColour[2]),i);
		}
		ImGui::Separator();
		ImGui::Text("LightPower            ");
		if (ImGui::InputFloat("pow", &currentPower))
		{
			m_scene->setLightPower(currentPower, i);
		}
		ImGui::Separator();
		ImGui::End();

	}
	return false;
}

void Application::DrawHierarchy()
{
}

void Application::PostProcessingUI()
{
	static bool postProcessUI = true;
	static bool enablePostProcessing = false;


	ImGui::Begin("Post Processing", &postProcessUI, ImGuiWindowFlags_AlwaysAutoResize);
	ImGui::SetWindowPos(ImVec2(m_scene->getScreenSize("Width") / 2 - 100, 0));
	//ImGui::
	ImGui::Checkbox("Enable/Disable", &enablePostProcessing);
	//if post processing enabled
	if (enablePostProcessing)
	{
		if (!m_scene->GetPostEffectsStatus())
		{
			m_scene->SetPostEffects(enablePostProcessing); //tell the scene the initialize the quad, render targets and necessary shaders.
		}
		//if any checkboxs ticked, set the the effect on and pass the bool value to the scene class inorder to bind the uniform.
		static bool depthOfField = false;
		static bool boxBlur = false;
		static bool distortion = false;
		static bool edgeDetection = false;
		static bool grayScale = false;
		static bool sepia = false;
		static float effectStrength = 1;
		ImGui::Separator();
		if (ImGui::Checkbox("Depth Of Field", &depthOfField))
		{
			m_scene->SetPostProcessEffect(depthOfField, 0);
		}
		ImGui::SameLine(0, 10);
		if (ImGui::Checkbox("Box Blur", &boxBlur))
		{
			m_scene->SetPostProcessEffect(boxBlur, 1);
		}
		ImGui::SameLine(0, 10);
		if (ImGui::Checkbox("Distortion", &distortion))
		{
			m_scene->SetPostProcessEffect(distortion, 2);
		}
		if (ImGui::Checkbox("Edge Detection", &edgeDetection))
		{
			m_scene->SetPostProcessEffect(edgeDetection, 3);
		}
		ImGui::SameLine(0, 10);
		if (ImGui::Checkbox("Gray Scale", &grayScale))
		{
			m_scene->SetPostProcessEffect(grayScale, 4);
		}
		ImGui::SameLine(0, 10);
		if (ImGui::Checkbox("Sepia", &sepia))
		{
			m_scene->SetPostProcessEffect(sepia, 5);
		}
		ImGui::Separator();
		if (ImGui::SliderFloat("Effect Strength", &effectStrength, 1, 100))
		{
			m_scene->setEffectStrength(effectStrength);
		}
	}
	else
	{
		if (m_scene->GetPostEffectsStatus())
		{
			m_scene->SetPostEffects(enablePostProcessing); //disable post processing.
		}
	}
	ImGui::End();
}



#pragma once
#include <vector>
#include "GameObject.h"
#include "Shader.h"
#include "Camera.h"
#include <aie/Gizmos.h>
#include <GLFW/glfw3.h>
#include "RenderTarget.h"
#include "Mesh.h"
#include "Debug.h"
#include "ParticleEmitter.h"


class GameObject;


using namespace std;
using namespace aie;
using namespace glm;
class Scene
{
public:
	//when creating a scene assign the light direction, the ambient light, the diffuse and the specular
	Scene(vec3 lightDirection, vec3 ambientLight, vec3 diffuse, vec3 specular, GLFWwindow* window) : m_lightDir(lightDirection), m_ambientLight(ambientLight), m_diffuse(diffuse), m_specular(specular), m_window(window)
	{
	}
	~Scene() {}

	//set the position of a light source at a provided index
	vec3 setLightPosition(vec3 position, int index) { return m_lightPos[index] = position; }
	//set the colour of a light source at a provided index
	vec3 setLightColour(vec3 colour, int index) { return m_lightColour[index] = vec3(colour.x/255, colour.y / 255, colour.z / 255); } //average the rgb values out to be between 0 - 1.
	//set the strength of a light source at a provided index
	float setLightPower(float power, int index) { return m_lightPower[index] = power; }
	//set the number of lights in scene
	int setNumOfLights(int num) { return m_numLights = num; }
	//get the number of lights in scene
	int getNumOfLights() { return m_numLights; }
	//get the position of a light source at a desired index
	vec3 getLightPosition(int index) { return m_lightPos[index]; }
	//get the position of a light colour at a desired index
	vec3 getLightColour(int index) { return m_lightColour[index]; }
	//get the strength of a light source at a desired index
	float getLightPower(int index) { return m_lightPower[index]; }
	//set the current effect to be either on or off.
	bool SetPostProcessEffect(bool status, int index) { return effectStatus[index] = status; }

	//intialize the quad or the unitialize.
	bool SetPostEffects(bool status) { return m_postProcessingEnabled = status, m_intializeQuad = status; }
	bool GetPostEffectsStatus() { return m_postProcessingEnabled; }
	//the strength of the post processing effect
	float setEffectStrength(float strength) { return strengthEffect = strength; }
	
	//update the scene
	void Update(float deltaTime);
	void ClearScreen();

	//draw the scene
	void Draw();
	
	//intialize the post process quad and render targets.
	void InitializePostProcessing();
	//delete the post process quad and render targets.
	void UnitializePostProcessing();
	//the currently selected object in the scene Hierachy
	GameObject* selectedObject(int index) { return gameObject[index]; }
	//gameObjects in scene
	vector<GameObject*> gameObject;
	//bind all the shaders (needs to be refactored so game objects do it themselves)
	void BindingUniforms(ShaderProgram* shader, GameObject* object);
	//get the size of the application window
	int getScreenSize(string HeightorWidth);
	Camera camera;

	//find a gameObject by tag
	GameObject* FindByTag(string tag);
	//find a gameObject by name
	GameObject* Find(string name);


protected:
	void DrawScene();

	//light properties
	vec3 m_lightPos[4];
	vec3 m_lightColour[4];
	float m_lightPower[4];
	int m_numLights;
	vec3 m_lightDir;
	vec3 m_ambientLight;
	vec3 m_diffuse;
	vec3 m_specular;
	//
	//post processing render target.
	RenderTarget* m_renderTarget;
	//depth of field effect render target
	RenderTarget* m_depthTarget;
	//the scree quad for post processing
	Mesh* m_postProcessQuad;
	bool m_postProcessingEnabled = false;
	bool m_intializeQuad = false;
	//6 effects, check which one is active
	bool effectStatus[6];
	float strengthEffect = 1;
	//the post processing effects shader
	ShaderProgram* m_postProcessingShader;
	ShaderProgram* m_depthShader;
	//Application display window.
	GLFWwindow* m_window;


};

